from typing import Any, Dict

from aiogram import Bot, Dispatcher
from aiogram.types import Update
from fastapi import APIRouter, BackgroundTasks, Body, Response, status

from app.bot.handlers import dp as dp
from app.core.config import settings

router = APIRouter()


async def feed_update(update: dict):
    telegram_update = Update(**update)
    Bot.set_current(dp.bot)
    Dispatcher.set_current(dp)
    await dp.process_update(telegram_update)


@router.post("/", include_in_schema=False)
async def telegram_post(
    background_tasks: BackgroundTasks, update: Dict[str, Any] = Body(...)
) -> Response:
    background_tasks.add_task(feed_update, update)
    return Response(status_code=status.HTTP_202_ACCEPTED)


@router.on_event("startup")
async def on_startup() -> None:
    Bot.set_current(dp.bot)
    Dispatcher.set_current(dp)
    current_url = (await dp.bot.get_webhook_info())["url"]
    if current_url != settings.WEBHOOK_URL:
        await dp.bot.set_webhook(url=settings.WEBHOOK_URL)


@router.on_event("shutdown")
async def on_shutdown() -> None:
    await dp.storage.close()
    await dp.storage.wait_closed()
    session = await dp.bot.get_session()
    await session.close()
