import hashlib
import time
from io import BytesIO

import requests
from bs4 import BeautifulSoup
from fastapi import APIRouter, BackgroundTasks, Depends, HTTPException, status
from fastapi.responses import JSONResponse
from fastapi.security import HTTPBasic, HTTPBasicCredentials
from pydantic import BaseModel, HttpUrl

from app.core.config import settings
from app.core.storage import CloudStorage
from app.parser.utils import normalize
from urllib.parse import urlparse
from requests.auth import HTTPBasicAuth

router = APIRouter()
security = HTTPBasic()

bucket = CloudStorage(
    settings.GOOGLE_CLOUD_STORAGE_BUCKET,
    settings.GOOGLE_CONFIG_PATH,
)


class Page(BaseModel):
    url: HttpUrl
    timestamped: bool = False
    def domain(self) ->str:
        return urlparse(self.url).netloc

    def sha1(self) -> str:
        return hashlib.sha1(self.url.encode("utf-8")).hexdigest()[:8]

    def parsable_url(self) -> str:
        return self.url.replace(
            "//meduza.io",
            "//website-light.preview.meduza.io",
        )

    def build_absolute_url(self, path: str) -> str:
        url = urlparse(self.parsable_url())
        return f"https://{url.hostname}{path}"

    def finale_filename(self) -> str:
        return f"{self.sha1()}.html"


def upload_image_to_bucket(image_url: str):
    bucket.upload_image(image_url)


@router.post("/conserve/", include_in_schema=False)
async def conserve_page(page: Page, background_tasks: BackgroundTasks):
    try:
        if "meduza.io" in page.url:
            response = requests.get(page.parsable_url(), auth=HTTPBasicAuth(settings.MDZA_BASIC_USERNAME, settings.MDZA_BASIC_PASSWORD))
        else:
            response = requests.get(page.parsable_url())
        soup = BeautifulSoup(response.text, "html.parser")

        if "meduza.io" in page.url:
            gs_script_tag = soup.new_tag("script")
            gs_script_tag["async"]="async"
            gs_script_tag["src"]='https://www.googletagmanager.com/gtag/js?id=G-L0LHMYHRHJ'
            soup.head.append(gs_script_tag)

            gs_tag = soup.new_tag("script")
            gs_tag.append("""
                window.dataLayer = window.dataLayer || [];
                function gtag(){dataLayer.push(arguments);}
                gtag('js', new Date());
                gtag('config', 'G-L0LHMYHRHJ', {'custom_map': {'dimension1': 'magicLink'}});
                gtag('event', 'magiclinkView', {'magicLink': true});
                """)
            soup.head.append(gs_tag)

            # remove existing google/robots meta tags
            existing_index_tags = soup.find_all('meta', content='index,follow')
            for tag in existing_index_tags:
                tag.extract()

            # add <meta name="robots" content="noindex">
            index_tag = soup.new_tag("meta")
            index_tag["name"]="robots"
            index_tag["content"]="noindex"
            soup.head.append(index_tag)

            root_a_tag = soup.find('a', class_='Link_root__2m5Bj')
            if root_a_tag:
                # Meduza wants header linked to their homepage
                root_a_tag['href'] = 'https://meduza.io'

        soup.head.append(soup.new_tag("style", type="text/css"))

        for link in soup.find_all("link"):
            rel = link.get("rel", [])

            if "stylesheet" in rel:
                path = link.get("href")
                absurl = page.build_absolute_url(path)
                cssfile_response = requests.get(absurl, auth=HTTPBasicAuth(settings.MDZA_BASIC_USERNAME, settings.MDZA_BASIC_PASSWORD))
                soup.head.style.append(cssfile_response.text.strip())

            link.decompose()

        for img in soup.find_all("img"):
            path = img.get("src")
            img["src"] = bucket.predict_image_public_url(path)
            background_tasks.add_task(upload_image_to_bucket, path)

        public_url = bucket.upload_file(
            file_name=page.finale_filename(),
            file_object=BytesIO(normalize(str(soup)).encode("utf-8")),
            content_type="text/html",
        )

        timestamp = int(time.time())

        if page.timestamped:
            public_url += f"?v={timestamp}"

        return JSONResponse(
            content={
                "url": public_url,
                "meta": {
                    "timestamp": timestamp,
                    "source_url": page.url,
                    "source_url_sha1": page.sha1(),
                },
            },
            status_code=status.HTTP_201_CREATED,
        )
    except Exception as _e:
        return JSONResponse(
            {"error": "unknown error"},
            status_code=status.HTTP_400_BAD_REQUEST,
        )
