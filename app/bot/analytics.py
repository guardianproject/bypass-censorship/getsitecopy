from datetime import datetime

from google.cloud.storage import Client

from app.core.config import settings


class Analyst:
    def __init__(self):
        self.client = Client.from_service_account_json(
            json_credentials_path=settings.GOOGLE_CONFIG_PATH,
        )
        self.bucket = self.client.get_bucket(settings.GOOGLE_CLOUD_STORAGE_BUCKET)
        self.folders = self.bucket.list_blobs()
        self.media = {}

    def month_analysts(self):
        current_datetime = datetime.now()
        media_count = {}

        for f in self.folders:
            name = f.name
            time_created = f.time_created

            if name.startswith("images/") or name.startswith("assets/"):
                continue

            hostname = name.split("/")[0]

            if hostname not in media_count:
                media_count[hostname] = [[], [], []]

            elif hostname in media_count:
                # Общее кол-во исп-ий за ВСЕ ВРЕМЯ
                media_count[hostname][0].append(time_created)
                if (
                    time_created.month == current_datetime.month
                    and time_created.year == current_datetime.year
                ):
                    # Общее кол-во исп-ий за МЕСЯЦ
                    media_count[hostname][1].append(time_created.month)
                    if time_created.day == current_datetime.day:
                        # Общее кол-во исп-ий за все ДЕНЬ
                        media_count[hostname][2].append(time_created.day)

        return media_count

    def all_rating(self):
        all_media = self.month_analysts()
        count = {}
        result = """"""
        number = 1
        for i, r in all_media.items():
            count[i] = len(r[0])

        sorted_count = sorted(count.items(), key=lambda x: x[1])
        sorted_count.reverse()

        for i, r in sorted_count:
            result = result + f"{number}. {i} - <code>{r}</code>\n\n"
            number += 1

        return result
