import asyncio

from aiogram import Bot, Dispatcher, types
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram.types import KeyboardButton, ReplyKeyboardMarkup
from trello import TrelloClient

from app.core.config import settings
from app.core.storage import gcs
from app.parser.exceptions import UnsupportedWebsite
from app.parser.parsers import create_parser_object

from .analytics import Analyst
from .messages import (
    ADMIN_ID,
    BOT_COMMANDS,
    FINAL_RESULT_MESSAGE,
    HELP_MESSAGE,
    MESSAGE_TEMPLATE,
    START_MESSAGE,
    USER_COMMANDS,
    QUESTION_CHOICE,
)

bot = Bot(token=settings.TELEGRAM_BOT_API_TOKEN)
dp = Dispatcher(bot, storage=MemoryStorage())


class OrderTask(StatesGroup):
    quest_or_problem = State()
    quest_message = State()
    help_message = State()
    original_link = State()
    copy_link = State()


@dp.message_handler(commands="start")
async def start(message: types.Message):
    markup = ReplyKeyboardMarkup(resize_keyboard=True)
    if str(message.from_user.id) in ADMIN_ID:
        button1 = KeyboardButton("Ratings")
        button2 = KeyboardButton("Statistics")
        markup.row(button1, button2)

    await message.answer(
        START_MESSAGE,
        parse_mode="Markdown",
        disable_web_page_preview=True,
        reply_markup=markup,
    )


@dp.message_handler(commands="help")
async def help(message: types.Message):  # noqa
    await message.answer(
        HELP_MESSAGE, parse_mode="Markdown", disable_web_page_preview=True
    )


@dp.message_handler(lambda msg: msg.text in ["Отмена", "/cancel"], state="*")
async def buttons(msg: types.Message, state: FSMContext):
    await state.finish()
    markup = ReplyKeyboardMarkup(resize_keyboard=True)
    if str(msg.from_user.id) in ADMIN_ID:
        button1 = KeyboardButton("Ratings")
        button2 = KeyboardButton("Statistics")
        markup.row(button1, button2)

    if msg.text in ["/cancel", "Отмена"]:
        await msg.answer(
            "Действие отменено",
            parse_mode="Markdown",
            disable_web_page_preview=True,
            reply_markup=markup,
        )

    if msg.text in ["Назад"]:
        await msg.answer(
            "Чтобы создать копию материала, отправьте боту ссылку",
            parse_mode="Markdown",
            disable_web_page_preview=True,
            reply_markup=markup,
        )

    if msg.text in ["/help"]:
        await msg.answer(
            HELP_MESSAGE,
            parse_mode="Markdown",
            disable_web_page_preview=True,
            reply_markup=markup,
        )

    if msg.text in ["/start"]:
        await start(msg)


@dp.message_handler(
    lambda msg: msg.text in ["Ratings"] and str(msg.from_user.id) in ADMIN_ID
)
async def rating(msg: types.Message):
    res = Analyst()
    result = res.all_rating()
    await msg.answer(
        f"<b>📊 Rating </b>\n\nAll-time bot usage rating:\n\n{result}",
        parse_mode="HTML",
        disable_web_page_preview=True,
    )


@dp.message_handler(
    lambda msg: msg.text in ["Statistics"] and str(msg.from_user.id) in ADMIN_ID
)
async def statistics(msg: types.Message):
    analys = Analyst()
    media_count = analys.month_analysts()

    await msg.answer(
        f"<b>Statistics:</b>\n\nGeneral statistics on the use of the bot",
        parse_mode="HTML",
        disable_web_page_preview=True,
    )

    for media, count in media_count.items():
        await msg.answer(
            f"<b>Name:</b> {media} \n\n"
            f"<b>Number of times used</b>\n"
            f"All time: {len(count[0])}\n"
            f"Per month: {len(count[1])}\n"
            f"Per day: {len(count[2])}",
            parse_mode="HTML",
        )
#
#
#@dp.message_handler(lambda msg: msg.text in ["Связаться с нами", "/media_help"], state="*")
#async def media_help_start(msg: types.Message, state: FSMContext):
#    quest = ReplyKeyboardMarkup(resize_keyboard=True)
#    quest_button_1 = KeyboardButton('Отмена')
#    #quest_button_2 = KeyboardButton('Задать вопрос')
#    #quest_button_3 = KeyboardButton('Проблемы с ботом')
#    quest.row(quest_button_1)
#    #quest.row(quest_button_2)
#    #quest.row(quest_button_3)
#
#    await msg.answer(f'{QUESTION_CHOICE}', parse_mode="HTML", reply_markup=quest)
#    await state.set_state(OrderTask.quest_or_problem.state)
#    return
#
#
#@dp.message_handler(state=OrderTask.quest_or_problem, content_types=["any"])
#async def quest_or_problem_func(msg: types.Message, state: FSMContext):
#    markup = ReplyKeyboardMarkup(resize_keyboard=True)
#    cancel_button = KeyboardButton("Отмена")
#    markup.row(cancel_button)
#
#    if msg.text in BOT_COMMANDS:
#        await buttons(msg)
#        return
#
#    elif msg.text not in ['Задать вопрос', 'Проблемы с ботом']:
#        await msg.answer(f'Пожалуйста, выберите один из вариантов')
#        return
#
#    elif msg.text in ['Проблемы с ботом']:
#        await state.update_data(quest_or_problem=msg.text)
#        await msg.answer(f"{MESSAGE_TEMPLATE}", reply_markup=markup)
#        await state.set_state(OrderTask.help_message.state)
#        return
#
#    if msg.text in ['Задать вопрос']:
#        await state.update_data(quest_or_problem=msg.text)
#        await msg.answer(f"Напишите после этого сообщения, с чем у вас возник вопрос:", reply_markup=markup)
#        await state.set_state(OrderTask.quest_message.state)
#        return
#
#
#@dp.message_handler(state=OrderTask.quest_message, content_types=["any"])
#async def media_quest_text(msg: types.Message, state: FSMContext):
#    if msg.text in BOT_COMMANDS:
#        await buttons(msg)
#        return
#
#    if msg.content_type != "text":
#        await msg.answer(f"Пожалуйста, отправьте текст")
#        return
#
#    if msg.text not in BOT_COMMANDS:
#        await state.update_data(quest_message=msg.text)
#        user_data = await state.get_data()
#        await msg.answer(
#            f"Отлично! Мы получили ваш вопрос, и скоро вам ответим!"
#        )
#        nikname = msg.from_user.username
#        quest_message = user_data.get("quest_message")
#        description = f"Текст: : {quest_message}\n\n"
#        description += f"Телеграм: @{nikname}"
#
#        client = TrelloClient(
#            api_key=settings.TRELLO_API_KEY,
#            api_secret=settings.TRELLO_API_SECRET,
#            token=settings.TRELLO_TOKEN,
#        )
#        trello_list = client.get_list(settings.TRELLO_LIST_ID)
#        trello_list.add_card(
#            name=f"Вопрос от: {nikname}",
#            desc=description,
#        )
#
#        msg.text = "Назад"
#        await buttons(msg)
#
#
#@dp.message_handler(state=OrderTask.help_message, content_types=["any"])
#async def media_help_text(msg: types.Message, state: FSMContext):
#    if msg.text in BOT_COMMANDS:
#        await buttons(msg)
#        return
#
#    if msg.content_type != "text":
#        await msg.answer(f"Пожалуйста, отправьте текст")
#        return
#
#    if msg.text not in BOT_COMMANDS:
#        await state.update_data(help_message=msg.text)
#        await msg.answer(f"Теперь отправьте ссылку на оригинальный материал, с которым возникли проблемы:")
#        await state.set_state(OrderTask.original_link.state)
#
#
#@dp.message_handler(state=OrderTask.original_link, content_types=["any"])
#async def media_help_original_url(msg: types.Message, state: FSMContext):
#    if msg.text in BOT_COMMANDS:
#        await buttons(msg)
#        return
#
#    if msg.content_type != "text":
#        await msg.answer(f"Пожалуйста, отправьте оригинальную ссылку")
#        return
#
#    if not msg.text.startswith("http"):
#        await msg.answer(f"Пожалуйста, отправьте оригинальную ссылку")
#        return
#
#    if msg.text not in BOT_COMMANDS:
#        await state.update_data(original_link=msg.text)
#        await msg.answer(f"Теперь отправьте ссылку на копию материала, в которой вы заметили проблемы:")
#        await state.set_state(OrderTask.copy_link.state)
#
#
#@dp.message_handler(state=OrderTask.copy_link, content_types=["any"])
#async def media_help_copy_url(msg: types.Message, state: FSMContext):
#    if msg.text in BOT_COMMANDS:
#        await buttons(msg)
#        return
#
#    if msg.content_type != "text":
#        await msg.answer(f"Пожалуйста, отправьте ссылку на копию")
#        return
#
#    if not msg.text.startswith("http"):
#        await msg.answer(f"Пожалуйста, отправьте ссылку на копию")
#        return
#
#    if msg.text not in BOT_COMMANDS:
#        await state.update_data(copy_link=msg.text)
#        user_data = await state.get_data()
#        await msg.answer(
#            f"Отлично! Мы получили ваше сообщение, и скоро с вами свяжемся!"
#        )
#        # await msg.answer(f"Текст: {user_data.get('help_message')}"
#        #                  f"\n\nСсылка на оригинал: {user_data.get('original_link')}"
#        #                  f"\nСсылка на копию: {user_data.get('copy_link')}"
#        #                  f"\n\nНикнейм: @{msg.from_user.username}"
#        #                  f"\nНазвание медиа: {user_data.get('original_link').split('/')[2]}")
#
#        media_name = user_data.get("original_link").split("/")[2]
#        if len(media_name) < 4:
#            media_name = "Unknown"
#
#        nikname = msg.from_user.username
#        help_message = user_data.get("help_message")
#        original_url = user_data.get("original_link")
#        copy_url = user_data.get("copy_link")
#
#        description = f"Медиа: {media_name}\n"
#        description += f"Телеграм: @{nikname}\n"
#        description += f"Текст: : {help_message}\n"
#        description += f"Оригинал: {original_url}\n"
#        description += f"Копия: : {copy_url}\n"
#
#        client = TrelloClient(
#            api_key=settings.TRELLO_API_KEY,
#            api_secret=settings.TRELLO_API_SECRET,
#            token=settings.TRELLO_TOKEN,
#        )
#        trello_list = client.get_list(settings.TRELLO_LIST_ID)
#        trello_list.add_card(
#            name=f"[Media] {media_name}",
#            desc=description,
#        )
#
#        msg.text = "Назад"
#        await buttons(msg)
#        return
#

async def parse_article_task(msg: types.Message):
    try:
        url = msg.text.strip()
        parser = await create_parser_object(url)
        file_object = parser.result()
        file_name = parser.page.filename()

        final_url = gcs.upload_file(
            file_name=file_name,
            file_object=file_object,
            content_type="text/html",
        )
        await msg.answer(f"{FINAL_RESULT_MESSAGE}")
        await msg.answer(
            f"<code>{final_url}</code>",
            parse_mode="HTML",
            disable_web_page_preview=False,
        )
    except UnsupportedWebsite:
        await msg.answer("Неподдерживаемый сайт.")
        # await msg.answer(
        #     SUPPORTED_PRESS,
        #     parse_mode="Markdown",
        #     disable_web_page_preview=True,
        # )


async def response_to_text(msg: types.Message):
    if not msg.text.strip().startswith("http"):
        return await msg.answer(
            "Не удалось обработать ссылку. Ссылка должна начинаться с *https://*",
            parse_mode="Markdown",
            disable_web_page_preview=True,
        )

    await msg.answer(
        f"Получаем страницу <b>{msg.text}</b>\n\nОжидайте...",
        parse_mode="HTML",
        disable_web_page_preview=True,
    )
    asyncio.create_task(parse_article_task(msg))


@dp.message_handler(content_types=["text"])
async def handler_url(msg: types.Message):
    if str(msg.from_user.id) in ADMIN_ID and msg.text not in BOT_COMMANDS:
        asyncio.create_task(response_to_text(msg))

    elif str(msg.from_user.id) not in ADMIN_ID and msg.text not in USER_COMMANDS:
        asyncio.create_task(response_to_text(msg))
