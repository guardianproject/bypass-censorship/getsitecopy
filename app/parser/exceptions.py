__all__ = ["UnsupportedWebsite", "ContentNotFound"]


class UnsupportedWebsite(Exception):
    pass


class ContentNotFound(Exception):
    pass
