from jinja2 import Environment, PackageLoader, select_autoescape

__all__ = ["jinja_env"]

jinja_env = Environment(
    loader=PackageLoader(
        package_name="app",
        package_path="templates",
    ),
    autoescape=select_autoescape(),
    trim_blocks=True,
    lstrip_blocks=True,
)
