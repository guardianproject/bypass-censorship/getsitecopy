import hashlib
import io
import typing as t

import aiohttp
import lxml.html
import requests
from bs4 import BeautifulSoup
from lxml.html import HTMLParser
from lxml.html.clean import Cleaner as HTMLCleaner

from app.core.storage import gcs
from app.parser.jinja import jinja_env
from app.core.config import settings
from .exceptions import UnsupportedWebsite
from .utils import extract_hostname, normalize

__all__ = ["create_parser_object"]

KeyFunction = t.Callable[[t.Any], t.Any]
XPathRule = dict[str, t.Any]
XPathRules = list[XPathRule]


class WebPage:
    def __init__(self, url: str, content: str) -> None:
        self.url = url
        self.soup = BeautifulSoup(content, "html.parser")

    def hostname(self) -> str:
        return extract_hostname(self.url)

    def filename(self):
        return f"{self.hostname()}/{self.sha1()[:8]}.html"

    def sha1(self) -> str:
        return hashlib.sha1(self.url.encode("utf-8")).hexdigest()

    def html(self, parser=None, encoding="utf-8") -> str:
        return lxml.html.fromstring(
            html=self.soup.prettify(encoding=encoding),
            parser=parser,
        )


class Parser:
    """Article content parser.

    All subclasses must implement the following attributes:

    title_xpath: XPath expression for the title of the article.
    article_xpath: XPath expression for the article content.
    timestamp_xpath: XPath expression for the article timestamp.
    remove_on_xpath_match: List of tuples of (tag, regex) for tags to exclude from article.

    See https://devhints.io/xpath for more info about xpath.
    """

    title_xpath: str | None = None
    tag_title_xpath: str | None = None
    remove_default_css: bool | None = None
    article_xpath: str
    image_preview_xpath: str | None = None
    timestamp_xpath: str | None = None
    remove_on_xpath_match: XPathRules = []
    page_template: str = "article-template.html"
    page_header_template: str | None = None
    page_footer_template: str | None = None
    google_analytics_tracking_id: str | None = None
    article_image_xpath: str | None = None
    description_xpath: str | None = None
    favicon_path: str | None = None
    custom_styles: list[str] = []
    custom_js: list[str] = []
    need_the_url: str | None = None
    styles_link: str | None = None
    encoding = "utf-8"
    bucket_name = settings.GOOGLE_CLOUD_STORAGE_BUCKET
    search_engine_indexing = True

    # Default HTML Parser:
    html_parser = HTMLParser(
        recover=True,
        remove_comments=True,
        remove_blank_text=True,
    )
    # Default HTML Cleaner:
    html_cleaner = HTMLCleaner(
        forms=False,
        scripts=False,
        embedded=False,
        annoying_tags=False,
        remove_unknown_tags=False,
        safe_attrs_only=False,
    )

    def __init__(self, web_page: WebPage) -> None:
        self.page = web_page
        self.html = self.page.html(
            parser=self.html_parser,
            encoding=self.encoding,
        )

    def find_all(self, selector: str, key: KeyFunction | None = None) -> t.Any:
        """Finds the first element matching the selector."""

        elements = self.html.xpath(selector)
        if len(elements) < 1:
            raise ValueError(f"No element found: {selector}")

        if not key:
            return elements
        return key(element)

    def find(self, selector: str, key: KeyFunction | None = None) -> t.Any:
        """Finds the first element matching the selector."""

        elements = self.html.xpath(selector)
        if len(elements) < 1:
            raise ValueError(f"No element found: {selector}")

        element = elements[0]

        if not key:
            return element
        return key(element)

    def get_og_image(self) -> t.Optional[str]:
        """Returns the image preview of the article."""
        try:
            preview = self.find(self.image_preview_xpath)
            return gcs.upload_image(preview)
        except BaseException:
            return None

    def get_article_image(self) -> t.Optional[str]:
        try:
            preview = self.find(self.article_image_xpath)
            return gcs.upload_image(preview)
        except BaseException:
            return None

    def get_timestamp(self) -> t.Union[str, None]:
        """Returns the timestamp of the article."""
        try:
            if not self.timestamp_xpath:
                return None

            result = self.find(self.timestamp_xpath, key=normalize)

            if result and "T" in result:
                return result.split("T")[0].strip()

            return None
        except:
            return None

    def get_title(self) -> str:
        """Returns the title of the article."""
        if self.title_xpath:
            title = self.find(self.title_xpath, key=normalize)
            if 'moscowtimes.com' in self.page.hostname():
                title = title.replace(" - The Moscow Times", "")
            if 'moscowtimes.ru' in self.page.hostname():
                title = title.replace(" - Русская служба The Moscow Times", "")
            return title

        else:
            return None

    def get_tag_title_xpath(self) -> t.Union[str, None]:
        """Returns the title of the article."""
        if self.tag_title_xpath:
            title = self.find(self.tag_title_xpath)
            title = lxml.html.tostring(
                doc=title,
                pretty_print=True,
                encoding="unicode",
            )
            return normalize(self.html_cleaner.clean_html(title))

        if not self.tag_title_xpath:
            return None

    def get_description(self) -> t.Union[str, None]:
        """Returns the description of the article."""
        try:
            return self.find(self.description_xpath, key=normalize)
        except:
            return None

    def get_need_the_url(self):
        url = self.need_the_url
        if url:
            return True
        else:
            return None

    def get_styles_link(self):
        if self.styles_link:
            custom_link_styles = []
            domain = f"https://{self.page.hostname()}"
            links = self.find(self.styles_link)

            for link in links:
                rel = link.get("rel")
                href = link.get("href")
                style = requests.get(f"{domain}{href}")

                if "stylesheet" in rel:
                    link_tag = f'<style type="text/css">{style.text.strip()}</style>'
                    link_formatted = lxml.html.fromstring(link_tag)
                    custom_link_styles.append(link_formatted)

            return custom_link_styles
        if not self.styles_link:
            return None

    def _replace_video_content(self, content) -> None:
        pass

    def _replace_audio_content(self, content) -> None:
        pass

    def _upload_images_and_replace(self, content) -> None:
        images = content.xpath(".//img[@src]|.//img[@data-src]")

        ignored_attributes = (
            "height",
            "id",
            "sizes",
            "srcset",
            "styles",
            "width",
        )

        if images:
            for image in images:
                src = image.attrib.get("src")

                if not src:
                    data_src = image.attrib.get("data-src")
                    if data_src:
                        src = data_src

                for attr in ignored_attributes:
                    image.attrib.pop(attr, None)

                # If the image is base64 encoded, we don't need to upload it.
                if src and not src.startswith("data:"):
                    # If src is relative, we need to make it absolute.
                    if src.startswith("/"):
                        src = f"https://{self.page.hostname()}{src}"
                    # Upload the image and replace the src attribute.
                    image.attrib.update({"src": gcs.upload_image(src)})

    def _remove_on_xpath_match(self, content) -> None:
        for rule in self.remove_on_xpath_match:
            xpath = rule.get("xpath", None)
            namespaces = None
            if "re:" in xpath:
                namespaces = {
                    "re": "http://exslt.org/regular-expressions",
                }
            for tag in content.xpath(xpath, namespaces=namespaces):
                #print(f"dropping tag {tag} based on rule {rule}")
                tag.drop_tree()

    @staticmethod
    def _remove_tag_attributes(content) -> None:
        content.attrib.clear()
        for element in content:
            element.attrib.clear()

    def _upload_links_and_replace(self, content):
        links = content.xpath(".//a[@href]")

        for link in links:
            href = link.attrib.get("href")

            if href.startswith("/"):
                href = f"https://{self.page.hostname()}{href}"
                link.attrib.update({"href": href})

    def get_content(self) -> str:
        """Returns the content of the article."""
        content = self.find(self.article_xpath)
        self._upload_images_and_replace(content)
        self._remove_on_xpath_match(content)
        self._upload_links_and_replace(content)
        self._replace_video_content(content)
        self._replace_audio_content(content)
        content = lxml.html.tostring(
            doc=content,
            pretty_print=True,
            encoding="unicode",
        )
        return normalize(self.html_cleaner.clean_html(content))

    def get_context(self) -> dict:
        """Returns the Jinja-context of the article."""
        return {
            "title": self.get_title(),
            "tag_title": self.get_tag_title_xpath(),
            "content": self.get_content(),
            "source_url": self.page.url,
            "need_the_url": self.get_need_the_url(),
            "favicon": self.favicon_path,
            "datetime": self.get_timestamp(),
            "source_name": self.page.hostname(),
            "article_url": self.page.filename(),
            "description": self.get_description(),
            "article_image": self.get_article_image(),
            "og_image": self.get_og_image(),
            "page_header_template": self.page_header_template,
            "page_footer_template": self.page_footer_template,
            "custom_styles": self.custom_styles,
            "custom_js": self.custom_js,
            "styles_link": self.get_styles_link(),
            "google_analytics_tracking_id": self.google_analytics_tracking_id,
            "bucket_name": self.bucket_name,
            "remove_default_css": self.remove_default_css,
            "index": self.search_engine_indexing
        }

    def result(self) -> t.IO:
        """Returns the article as a file."""
        context = self.get_context()
        template = jinja_env.get_template(self.page_template)
        html = template.render(**context).encode("utf-8")
        return io.BytesIO(html)

class OpenGraphXPathMixin:
    title_xpath = '//meta[@property="og:title"]/@content'
    tag_title_xpath = None
    styles_link = None
    need_the_url = True
    image_preview_xpath = '//meta[@property="og:image"]/@content'
    description_xpath = '//meta[@property="og:description"]/@content'
    page_footer_template = "footer/base_footer.html"

class ArticleParser(OpenGraphXPathMixin, Parser):
    pass

class HolodParser(ArticleParser):
    title_xpath = "//meta[@property='og:title']/@content"
    article_xpath = "//div[contains(@class, 'article__content')]"
    timestamp_xpath = "//meta[@property='article:published_time']/@content"
    article_image_xpath = "//article[contains(@class, 'article')]//img[contains(@class, 'pic-holder')]/@src"
    image_preview_xpath = '//meta[@property="og:image"]/@content'
    page_header_template = "headers/holod.html"
    favicon_path = "assets/favicons/holod.ico"
    google_analytics_tracking_id = "UA-144382294-1"
    custom_styles = ["assets/css/holod.css"]
    custom_js = ["assets/js/holod/app.js"]
    search_engine_indexing = False
    remove_on_xpath_match = [
        {
            "xpath": ".//*[re:match(@class, 'm-block-inlinemore')]",
        },
        {
            "xpath": '//span[contains(@class, "texttoggle__icon")]',
        },
    ]


class BBCRussianParser(ArticleParser):
    encoding = None
    article_xpath = '//main[contains(@role, "main")]'
    timestamp_xpath = "//meta[@name='article:published_time']/@content"
    image_preview_xpath = '//meta[@property="og:image"]/@content'
    article_image_xpath = None
    page_header_template = "headers/bbc.html"
    favicon_path = "assets/favicons/bbcrussian.ico"
    custom_styles = ["assets/fonts/bbc/fonts.css", "assets/css/bbc.css"]
    remove_on_xpath_match = [
        {
            "xpath": '//div[contains(@class, "bbc-1151pbn ebmt73l0")]',
        },
        {
            "xpath": '//div[contains(@class, "e1j2237y7 bbc-q4ibpr ebmt73l0")]',
        },
        {
            "xpath": '//div[contains(@class, "bbc-1151pbn ebmt73l0")]',
        },
        {
            "xpath": '//div[contains(@class, "bbc-zvnee0 e1rfboeq6")]',
        },
        {
            "xpath": '//div[contains(@class, "e1wxwc760 bbc-1bh6w8m ebmt73l0")]',
        },
        {
            "xpath": '//div[contains(@class, "bbc-4quw3x e1rfboeq7")]',
        },
        {
            "xpath": '//div[contains(@class, "e1rfboeq6 bbc-12e4ke4 e1s8ztj50")]',
        },
    ]

    def _upload_images_and_replace(self, content) -> None:
        figures = content.xpath(".//figure[contains(@class, 'bbc-1qn0xuy')]")

        for figure in figures:
            images = figure.xpath(".//img[@src]") or []

            for image in images:
                src = image.attrib.get("src")
                img_tag = f'<img src="{src}" class="bbc_img">'
                data_formatted = lxml.html.fromstring(img_tag)
                figure.getparent().replace(figure, data_formatted)

                if src and not src.startswith("data:"):
                    if src.startswith("/"):
                        src = f"https://{self.page.hostname()}{src}"
                    figure.attrib.update({"src": gcs.upload_image(src)})


class SkatMediaParser(ArticleParser):
    encoding = None
    article_xpath = '//div[contains(@class, "content_article")]'
    timestamp_xpath = '//time[contains(@class, "date")]/text()'
    page_header_template = "headers/skat.html"
    favicon_path = "assets/favicons/skat.ico"
    image_preview_xpath = '//meta[@property="og:image"]/@content'
    article_image_xpath = '//meta[@property="og:image"]/@content'
    custom_styles = ["assets/css/skat.css"]
    google_analytics_tracking_id = "UA-57428004-1"
    search_engine_indexing = False
    remove_on_xpath_match = [
        {
            "xpath": '//h1[contains(@class, "title")]',
        },
        {
            "xpath": '//time[contains(@class, "date")]',
        },
        {
            "xpath": '//div[contains(@class, "meta")]',
        },
        {
            "xpath": '//div[contains(@class, "support_container")]',
        },
        {
            "xpath": '//div[contains(@class, "share_container")]',
        },
        {
            "xpath": '//img[contains(@class, "load_container")]',
        },
        {
            "xpath": '//div[contains(@class, "client-only-placeholder")]',
        },
        {
            "xpath": '//img[@src = "//:0"]',
        },
    ]



class MTRParser(ArticleParser):
    page_header_template = "headers/moscowtimes.html"
    remove_default_css = True
    title_xpath = '//head/title/text()'
    article_xpath = "//article[contains(@class,'article article--')]"
    remove_on_xpath_match = [
        {
        "xpath": ".//*[re:match(@class, 'article__header|byline__social|article__related-article|article__bottom|article__tags|social social--buttons')]"},
        {"xpath": '//div[contains(@class, "hidden")]'},
        {"xpath": '//div[contains(@class, "social")]'},
        {"xpath": '//div[contains(@class, "related-article")]'},
            {"xpath": '//div[contains(@class, "author-excerpt")]'}
    ]
    description_xpath = "//meta[@property='og:description']"
    image_preview_xpath = "//meta[@name='thumbnail']/@content"
    timestamp_xpath = "//time/@datetime"
    custom_styles = ["assets/css/mt_custom.css"]
    google_analytics_tracking_id = "G-770VB7H0WN"

PARSERS = {
    "holod.media": HolodParser,
    "www.moscowtimes.ru": MTRParser,
    "www.themoscowtimes.com": MTRParser,
    "www.bbc.com": BBCRussianParser,
    "skat.media": SkatMediaParser,
}


async def create_parser_object(url) -> Parser:
    url = url.strip()
    # user_agent = {'user-agent': 'Googlebot/2.1 (+http://www.google.com/bot.html)'}
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as response:
            text_content = await response.text()
            webpage = WebPage(url, text_content)

            hostname = webpage.hostname()
            parser_class = PARSERS.get(hostname)
            if parser_class is None:
                raise UnsupportedWebsite(f"Unknown hostname: {hostname}")
            return parser_class(webpage)
