import re
import unicodedata
from urllib.parse import urlparse


def extract_hostname(url: str) -> str:
    """Extracts the hostname from a URL."""
    hostname = urlparse(url).hostname

    if not hostname:
        return ""

    return hostname


def rm_whitespaces(text: str) -> str:
    """Removes whitespace from a string."""
    return re.sub(r"\s\s+", " ", text).strip()


def normalize(text: str) -> str:
    """Removes whitespace from a string."""
    return unicodedata.normalize("NFKD", rm_whitespaces(text))
