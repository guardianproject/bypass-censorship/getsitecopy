from pathlib import Path
from typing import Any, Dict, Optional

from pydantic import BaseSettings, PostgresDsn, validator

BASE_DIR = Path(__file__).resolve().parent.parent.parent
PROJ_DIR = BASE_DIR.parent.parent  # versions/<commit_hash>


class Settings(BaseSettings):
    PROJECT_NAME: str
    POSTGRES_SERVER: str
    POSTGRES_USER: str
    POSTGRES_PASSWORD: str
    POSTGRES_DB: str
    DATABASE_URI: Optional[PostgresDsn] = None

    @validator("DATABASE_URI", pre=True)
    def assemble_db_connection(cls, v: Optional[str], values: Dict[str, Any]) -> Any:
        if isinstance(v, str):
            return v
        return PostgresDsn.build(
            scheme="postgresql",
            user=values.get("POSTGRES_USER"),
            password=values.get("POSTGRES_PASSWORD"),
            host=values.get("POSTGRES_SERVER"),
            path=f"/{values.get('POSTGRES_DB') or ''}",
        )

    TELEGRAM_BOT_API_TOKEN: str
    WEBHOOK_URL: str

    BASE_DIR = BASE_DIR

    GOOGLE_CONFIG_PATH = str(PROJ_DIR.joinpath(".google-config.json"))
    GOOGLE_CLOUD_STORAGE_BUCKET: str
    # XXX: just making it "str" as others doesn't work 🤷
    DEFAULT_CSS_FILE = BASE_DIR.joinpath("app", "parser", "static", "css", "styles.css")

    MDZA_BASIC_USERNAME: Optional[str]
    MDZA_BASIC_PASSWORD: Optional[str]

    # Do not modify this line
    # GOOGLE_CLOUD_STORAGE_MEDUZA_BUCKET = "mdza"

    # Trello login details
    TRELLO_API_KEY: Optional[str]
    TRELLO_API_SECRET: Optional[str]
    TRELLO_TOKEN: Optional[str]
    TRELLO_BOARD_ID: Optional[str]
    TRELLO_LIST_ID: Optional[str]

    class Config:
        case_sensitive = True
        env_file = str(PROJ_DIR.joinpath(".env"))
        # env_file = ".env"


settings = Settings()
