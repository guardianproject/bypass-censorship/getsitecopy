from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from app.core.config import settings
from app.routers import api, telegram


def get_application() -> FastAPI:
    _app = FastAPI(title=settings.PROJECT_NAME)
    _app.include_router(telegram.router, prefix="/bot")
    _app.include_router(api.router, prefix="/api")

    _app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_methods=["*"],
        allow_headers=["*"],
        allow_credentials=True,
    )

    return _app


app = get_application()
