FROM python:3.11

# Install dependencies
RUN apt-get update && pip install poetry

# Build
COPY . /gsc/
WORKDIR /gsc/
RUN poetry lock && poetry install

CMD poetry run uvicorn app.main:app --reload --host 0.0.0.0
